// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEG_API ASnakeGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
